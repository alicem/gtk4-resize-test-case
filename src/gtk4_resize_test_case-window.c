
#include "gtk4_resize_test_case-config.h"
#include "gtk4_resize_test_case-window.h"

struct _Gtk4ResizeTestCaseWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkLabel            *label;
};

G_DEFINE_TYPE (Gtk4ResizeTestCaseWindow, gtk4_resize_test_case_window, GTK_TYPE_APPLICATION_WINDOW)

static void
gtk4_resize_test_case_window_class_init (Gtk4ResizeTestCaseWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/App/gtk4_resize_test_case-window.ui");
  gtk_widget_class_bind_template_child (widget_class, Gtk4ResizeTestCaseWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, Gtk4ResizeTestCaseWindow, label);
}

static void
gtk4_resize_test_case_window_init (Gtk4ResizeTestCaseWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

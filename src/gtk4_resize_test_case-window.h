
#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTK4_RESIZE_TEST_CASE_TYPE_WINDOW (gtk4_resize_test_case_window_get_type())

G_DECLARE_FINAL_TYPE (Gtk4ResizeTestCaseWindow, gtk4_resize_test_case_window, GTK4_RESIZE_TEST_CASE, WINDOW, GtkApplicationWindow)

G_END_DECLS
